#![forbid(unsafe_code)]

use std::{io, process::exit};

fn fibonacci(n: u32) -> u128 {
    if n <= 0 {
        return 0;
    } else if n == 1 {
        return 1;
    }

    return fibonacci(n - 1) + fibonacci(n - 2);
}

fn main() {
    println!("Generate the nth Fibonacci Number");
    println!("Input a number (Larger numbers take a LOT of time)");

    // Copy pasted from guessing-game
    let mut input = String::new();

    io::stdin()
        .read_line(&mut input)
        .expect("Failed to readline");

    let input: u32 = match input.trim().parse() {
        Ok(num) => num,
        Err(_) => exit(1),
    };

    for iter in 1..input {
        println!(
            "[{}/{}] The {}th fibonacci number is: {}",
            iter,
            input,
            iter,
            fibonacci(iter)
        );
    }
}

# Fibonacci Generator

Generates the nth Fibonacci number, using recursion (very slow).

## Usage

Give it a number and it'll calculate it (from 1st to nth)

```bash
Generate the nth Fibonacci Number
Input a number (Larger numbers take a LOT of time)
10
[1/10] The 1th fibonacci number is: 1
[2/10] The 2th fibonacci number is: 1
[3/10] The 3th fibonacci number is: 2
[4/10] The 4th fibonacci number is: 3
[5/10] The 5th fibonacci number is: 5
[6/10] The 6th fibonacci number is: 8
[7/10] The 7th fibonacci number is: 13
[8/10] The 8th fibonacci number is: 21
[9/10] The 9th fibonacci number is: 34
```

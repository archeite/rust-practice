#![forbid(unsafe_code)]

fn main() {
    // Variables
    let ordinal_numbers = [
        "first", "second", "third", "fourth", "fifth", "sixth", "seventh", "eighth", "ninth",
        "tenth", "eleventh", "twelfth",
    ];

    let objects = [
        "",
        "Two turtle doves",
        "Three French hens",
        "Four calling birds",
        "Five golden rings",
        "Six geese a-laying",
        "Seven swans a-swimming",
        "Eight maids a-milking",
        "Nine ladies dancing",
        "Ten lords a-leaping",
        "Eleven pipers piping",
        "Twelve drummers drumming",
    ];

    let mut day = 0;
    let mut object_iter; 

    // Main Loop
    while day < 12 {        
        // Print newline to seperate between days
        println!("");

        println!("On the {} day of Christmas", ordinal_numbers[day]);
        println!("My true love gave to me");
        
        if day == 0 {
            println!("A partridge in a pear tree.");
        } else {
            object_iter = day;

            // Loop and decrement object_iter, so it counts down
            while object_iter != 0 {
                println!("{},", objects[object_iter]);
                object_iter -= 1;
            }
            println!("And a partridge in a pear tree.");

        }

        // Increment by one, reset and loop back
        day += 1;
    }
}

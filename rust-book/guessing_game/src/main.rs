use std::io;
use rand::Rng;

fn main() {
    println!("Guess the number! (Legal Gambling)");

    let secret_number = rand::thread_rng().gen_range(1..=100);

    //println!("Thou secret number is {secret_number}");
    loop {
        println!("Please input your guess.");

        let mut guess = String::new();

        io::stdin()
            .read_line(&mut guess)
            .expect("Failed to readline");

        let guess: u32 = match guess.trim().parse(){
            Ok(num) => num,
            Err(_) => continue,
        };

        println!("You have guessed: {}", guess);

        match guess.cmp(&secret_number) {
            std::cmp::Ordering::Less    => println!("Your guess is too small!"),
            std::cmp::Ordering::Equal   => {
                println!("You got it right! You win nothing!");
                break;
            }
            std::cmp::Ordering::Greater => println!("Your guess is too big!"),
        }
    }
}

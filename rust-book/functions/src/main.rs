fn main() {
    println!("Hello, world!");

    another_function(200, 'g');
}

fn another_function(x: u8, display: char) {
    println!("Another function. Also x is: {x} with unit {display}");
}

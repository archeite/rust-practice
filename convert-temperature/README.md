# Fahrenheit and Celsius Converter

Rust program that converts between Fahrenheit and Celsius temperature scales.

## Usage

Append `C` or `F` at the end of the input to specify the temperature your
converting from.

```rs
// Celsius to Fahrenheit
Input:  0C
Output: 0C = 32F

// Fahrenheit to Celsius
Input:  32F
Output: 32F = 0C
```

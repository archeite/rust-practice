#![forbid(unsafe_code)]
use std::io;

fn convert_temperature(value: f64, unit: char) -> f64 {
    if unit == 'C' {
        9.0 / 5.0 * value + 32.0
    } else if unit == 'F' {
        (5.0 / 9.0) * (value - 32.0)
    } else {
        0.0
    }
}

fn main() {
    println!("Fahrenheit and Celsius Converter");

    loop {
        // Print newline to seperate conversions
        println!("");

        // Get input from user
        let mut user_input: String = String::new();
        io::stdin()
            .read_line(&mut user_input)
            .expect("Failed to readline");

        // Parse input
        let mut user_input: String = match user_input.trim().parse() {
            Ok(string) => string,
            Err(_) => continue,
        };

        // Seperate inputs into the unit and the value
        let temperature_unit: char = user_input.chars().last().unwrap();
        user_input.pop();
        let temperature_value: f64 = match user_input.trim().parse() {
            Ok(num) => num,
            Err(_) => continue,
        };

        let converted_unit: char = if temperature_unit == 'F' {
            'C'
        } else if temperature_unit == 'C' {
            'F'
        } else {
            eprintln!("Please specify a unit (C/F)");
            continue;
        };

        let calculated_temperature: f64 = convert_temperature(temperature_value, temperature_unit);

        // Output conversion
        println!(
            "{}{} = {}{}",
            temperature_value, temperature_unit, calculated_temperature, converted_unit
        );
    }
}
